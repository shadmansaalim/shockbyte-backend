//Imports
const express = require('express');
const { MongoClient } = require('mongodb');
const cors = require('cors');
require('dotenv').config()
const ObjectId = require('mongodb').ObjectId;
const admin = require("firebase-admin");


const app = express();
const port = process.env.PORT || 5000;


//Firebase Admin Initialization
const serviceAccount = require('./shockbyte-coding-test-firebase-adminsdk-wpdqp-84f8be61e7.js');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});



//Middleware use for server
app.use(cors());
app.use(express.json());


//MongoDB
const uri = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@cluster0.jqer6.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`;
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });


//Function to verify user using JWT token
async function verifyToken(req, res, next) {
    console.log(req.headers)
    if (req.headers?.authorization?.startsWith('Bearer ')) {
        const token = req.headers.authorization.split(' ')[1];

        try {
            const decodedUser = await admin.auth().verifyIdToken(token);
            req.decodedEmail = decodedUser.email;
        }
        catch {

        }
    }
    next();
}


async function run() {
    try {
        await client.connect();
        const database = client.db("shockbyte");
        const usersCollection = database.collection("users");


        //Get all users
        app.get('/users', async (req, res) => {
            const cursor = usersCollection.find({});
            const users = await cursor.toArray();
            res.json(users);
        })

        //Checking if user is admin or not
        app.get('/users/:email', async (req, res) => {
            const email = req.params.email;
            const query = { email: email };
            const user = await usersCollection.findOne(query);
            let isAdmin = false;
            if (user?.role === 'Admin') {
                isAdmin = true;
            }
            res.json({ admin: isAdmin });
        })

        //Add users to database those who signed up with Email Password
        app.post('/users', async (req, res) => {
            const user = req.body;
            //Default user type
            user.role = 'Member';
            const result = await usersCollection.insertOne(user);
            res.json(result);
        })

        app.put('/users/admin', verifyToken, async (req, res) => {
            const user = req.body;
            const type = user.type;
            const requester = req.decodedEmail;
            console.log(requester)
            if (requester) {
                const requesterAccount = await usersCollection.findOne({ email: requester });
                if (requesterAccount.role === 'Admin') {
                    const filter = { email: user.email };
                    const updateDoc = { $set: { role: type } };
                    const result = await usersCollection.updateOne(filter, updateDoc);
                    console.log(result);
                    res.json(result);
                }
            }
            else {
                res.status(403).json({ message: 'You do not have access to make admin power' })
            }

        })

    }
    finally {
        //   await client.close();
    }
}
run().catch(console.dir);

app.get('/', (req, res) => {
    console.log('Hitting backend');
    res.send('Shockbyte Coding Test Backend')
})

app.listen(port, () => {
    console.log('Listening to port number ', port);
})

